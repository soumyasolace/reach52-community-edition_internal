var mongoose = require("mongoose");
var paymentTransactionSchema = new mongoose.Schema(
    {
        _partition: {
            type: String,
            default: "101"
        },
        paymentId: {
            type: String,
            required: true,
        },
        orderId: {
            type: String,
            required: true,
        },
        userId: {
            type: String,
            required: true,
        },
        agentId: {
            type: String,
            required: true,
        },
        residentId: {
            type: String,
            required: true,
        },
        isHigh: {
            type: Boolean,
            default: false
        },
        orderStatusUpdated: {
            type: Boolean,
            default: false
        },
        creditCardChargeId: {
            type: String,
        },
        paymentMethod: {
            type: String,
            default: ""
        },
        status: {
            type: String,
        },
        merchantName: {
            type: String,
            default: ""
        },
        amount: {
            type: mongoose.Decimal128,
            default: 0.00
        },
        paidAmount: {
            type: mongoose.Decimal128,
            default: 0.00
        },
        paidAt: {
            type: Date,
        },
        payerEmail: {
            type: String,
        },
        description: {
            type: String,
        },
        adjustedReceivedAmount: {
            type: mongoose.Decimal128,
            default: 0.00
        },
        currency: {
            type: String,
        },
        paymentChannel: {
            type: Array,
            default: []
        },
    },
    {
        timestamps: {
            createdAt: "createdAt",
            updatedAt: "updatedAt",
        },
    }
);
var customeCollectionName = 'PaymentTransaction'
/// TO MAKE CUSTOME COLLECTION NAME
module.exports = mongoose.model("paymentTransactions", paymentTransactionSchema, customeCollectionName);
