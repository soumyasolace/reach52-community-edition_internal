const { createCharge, captureCharge, getCharge, createToken } = require('./charge');
const { promWithJsErr, fetchWithHTTPErr, Auth, Validate } = require('../utils');
const {
  createAuthorization,
  reverseAuthorization,
} = require('./authorization');
const { createRefund } = require('./refund');
const CardStatus = require('./card_status');
const dotenv = require('dotenv');
const CARD_PATH = '';

function Card(options) {
  let aggOpts = options;
  if (Card._injectedOpts && Object.keys(Card._injectedOpts).length > 0) {
    aggOpts = Object.assign({}, options, Card._injectedOpts);
  }

  this.opts = aggOpts;
  this.API_ENDPOINT = this.opts.xenditURL + CARD_PATH;
}

Card._injectedOpts = {};
Card._constructorWithInjectedXenditOpts = function (options) {
  Card._injectedOpts = options;
  return Card;
};



Card.prototype.createToken = function (data) {
  return promWithJsErr((resolve, reject) => {
    // const compulsoryFields = [];

    // Validate.rejectOnMissingFields(compulsoryFields, data, reject);

    // fetchWithHTTPErr(`${this.API_ENDPOINT}/customers`, {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     Authorization: Auth.basicAuthHeader(this.opts.secretKey),
    //   },

    console.log('dddddddddd123', data)
    console.log('eeeeeeee123', this.opts)
    //xnd_public_development_NUwpr0CxEVTMXjAL9M9lONxOiS7mrPQXs4Vp4qe6jGsdfl9HgViXu3vp1e8kA
    const compulsoryFields = ['amount'];
    Validate.rejectOnMissingFields(compulsoryFields, data, reject);
    var finalData = JSON.stringify({
      amount: data.amount,
      card_data: data.card_data,
      card_cvn: data.card_cvn,
      is_single_use: data.is_single_use,
      should_authenticate: data.should_authenticate,
      billing_details: data.billing_details
    })
    console.log('ttttttttttt123',finalData,this.opts.secretKey)
    //https://api.xendit.co/v2/credit_card_tokens
    // fetchWithHTTPErr('https://api.xendit.co/v2/credit_card_tokens', {
    //   method: 'POST',
    //   headers: {
    //     Authorization: Auth.basicAuthHeader('xnd_public_development_NUwpr0CxEVTMXjAL9M9lONxOiS7mrPQXs4Vp4qe6jGsdfl9HgViXu3vp1e8kA'),
    //     'Content-Type': 'application/json',
    //   },

    //   body:finalData,
    // })
    fetchWithHTTPErr(`${process.env.XENDIT_V2_URL}/credit_card_tokens`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: Auth.basicAuthHeader(this.opts.secretKey),
      },
      body: finalData,
    })
      .then((res) => {
        console.log(res)
        resolve(res);
      })
      .catch((err) => {
        console.log(err)
        reject(err);
      });
  });
};





Card.Status = CardStatus;

Card.prototype.createCharge = createCharge;
Card.prototype.captureCharge = captureCharge;
Card.prototype.getCharge = getCharge;
Card.prototype.createAuthorization = createAuthorization;
Card.prototype.reverseAuthorization = reverseAuthorization;
Card.prototype.createRefund = createRefund;

module.exports = Card;
