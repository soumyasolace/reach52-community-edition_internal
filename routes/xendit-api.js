module.exports = (app) => {
    const x = require('../with_async/xendit');
    var ObjectId = require('mongodb').ObjectID;
    const order = require('../models/order-schema');
    const paymentTransaction = require('../models/paymentTransaction');
    var orderModule = require('../module/order_module')();
    const { Customer } = x;
    app.post('/api/payment/invoice/create', async function (req, res) {
        try {
            if (!req.body.orderId) {
                res.status(400).json({ status: false, message: "orderId parameter is missing" });
                return;
            }
            if (!req.body.agentId) {
                res.status(400).json({ status: false, message: "agentId parameter is missing" });
                return;
            }
            if (!req.body.residentId) {
                res.status(400).json({ status: false, message: "residentId parameter is missing" });
                return;
            }
            if (!req.body.amount) {
                res.status(400).json({ status: false, message: "amount parameter is missing" });
                return;
            }
            if (!req.body.description) {
                res.status(400).json({ status: false, message: "description parameter is missing" });
                return;
            }
            if (!req.body.payerEmail) {
                res.status(400).json({ status: false, message: "payerEmail parameter is missing" });
                return;
            }
            const { Invoice } = x;
            const invoiceSpecificOptions = {};
            const i = new Invoice(invoiceSpecificOptions);
            const resp = await i.createInvoice({
                externalID: req.body.orderId + "_" + req.body.agentId + "_" + req.body.residentId,
                amount: req.body.amount,
                payerEmail: req.body.payerEmail,
                description: req.body.description,
                should_send_email: true,
                customer_notification_preference: {
                    invoice_paid: ["email"]
                }
            });
            res.status(200).json({ status: true, message: 'Invoice created', data: resp });
        }
        catch (er) {
            res.status(500).json({ status: false, message: er });
        }
    });
    app.post('/api/payment/invoice/status', async function (req, res) {
        try {
            if (process.env.X_CALLBACK_TOKEN === req.headers['x-callback-token']) {
                var a = req.body.external_id.split("_")
                const Data = {
                    _partition: a[1],
                    agentId: a[1],
                    residentId: a[2],
                    paymentId: req.body.id,
                    orderId: a[0],
                    userId: req.body.user_id,
                    isHigh: req.body.is_high,
                    creditCardChargeId: req.body.credit_card_charge_id,
                    paymentMethod: req.body.payment_method,
                    status: req.body.status,
                    merchantName: req.body.merchant_name,
                    amount: req.body.amount,
                    paidAmount: req.body.paid_amount,
                    paidAt: req.body.paid_at,
                    payerEmail: req.body.payer_email,
                    description: req.body.description,
                    adjustedReceivedAmount: req.body.adjusted_received_amount,
                    currency: req.body.currency,
                    paymentChannel: req.body.payment_channel,
                    orderStatusUpdated: false
                };
                // SAVING  DETAILS IN PAYMENT TRANSACTION COLLECTION
                const paymentDetails = new paymentTransaction(Data);
                let payData = await paymentDetails.save()
                if (payData) {
                    const query = { _id: new ObjectId(a[0]) };
                    const updateDocument = {
                        $set: { "subOrders.$[].paymentStatus": "PAID" }
                    };
                    const result = await order.updateOne(query, updateDocument);
                    if (result.nModified) {
                        // IF THAT ORDER ID IS UPDATED IN ORDERMEDICINE COLLECTION THEN ONLY UPDATE ORDERSTATUSUPDATED TO TRUE
                        const updatePaymenTransaction = await paymentTransaction.updateOne({ _id: new ObjectId(payData._id) },
                            {
                                $set: { orderStatusUpdated: true }
                            })
                    }
                }
            }
            else {
                res.status(200).json({ status: false, message: 'Unauthorized transaction' });
            }
            res.status(200).json({ status: true, message: 'Payment completed' });
        }
        catch (er) {
            res.status(500).json({ status: false, message: er });
        }
    });
    // cron job test api for update order payment status
    app.post('/api/xendit/updateorderpaymentstatus', function (req, res) {
        try {
            orderModule.updateOrderPaymentStatus(
                function (error, message) {
                    if (error) {
                        res.status(500).json({
                            status: false,
                            message: message,
                        })
                    }
                    else {
                        res.status(200).json({
                            status: true,
                            message: message,
                        })
                    }
                })
        }
        catch (er) {
            res.status(500).json({ status: false, message: er });
        }
    });
};
